#include <string.h>
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include "TTT.h"
#include "simple_com.h"
#include <libgen.h>
#include "LinuxDARwIn.h"
#include "MotionStatus.h"

#ifdef MX28_1024
#define MOTION_FILE_PATH    "/home/darwin/TicTacToe/Data/motion_1024.bin"
#else
#define MOTION_FILE_PATH    "/home/darwin/TicTacToe/Data/motion_4096.bin"
#endif

using namespace std;
using namespace Robot;

void playmove( int n );
int executeScript();

int is_master;

int main( int argc, char *argv[]) {
	//
	// Set up Everything
	//

	//Process command line input
	

	//Load the Motion File
	Action::GetInstance()->LoadFile( MOTION_FILE_PATH );
	//////////////////// Framework Initialize ////////////////////////////
	LinuxCM730 linux_cm730("/dev/ttyUSB1");
	CM730 cm730(&linux_cm730);
	if(MotionManager::GetInstance()->Initialize(&cm730) == false)
	{
		cout << "Fail to connect CM-730!" << endl;
	}
	MotionManager::GetInstance()->AddModule((MotionModule*)Action::GetInstance());
	LinuxMotionTimer *motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
	motion_timer->Start();
	MotionManager::GetInstance()->SetEnable(true);
	/////////////////////////////////////////////////////////////////////


	// Set the darwin to enter the standing(init) position
	Action::GetInstance()->Start(1);
	while(Action::GetInstance()->IsRunning()) usleep(8*1000);

	// Initiallize Zigby communication
	system( "sh /home/darwin/TicTacToe/Linux/project/TTT/setup-ttyUSB.bash" );
	comm_init();
	/* is_master is an integer value with equaling -1, 0, or 1
	 *  0 = master
	 *  1 = slave
	 * -1 = Failure
	*/
	while( 1 ) {
		
		usleep( 100 );
		
		if(MotionStatus::BUTTON == 2)
		{
			executeScript();
		}
	}

}



int executeScript() {

	// Set the darwin to enter the standing(init) position
	Action::GetInstance()->Start(1);
	while(Action::GetInstance()->IsRunning()) usleep(8*1000);

	is_master = comm_start( 0 );	
	
	//
	// Create and execute the script
	//
	unsigned char _message[1] = {""};
	int n;
	switch( is_master ) {

		/*
		 * If the communication fails the code will exit and return -1;
		 */  
		case -1:
			// Communication failed
			return -1;
		
		/*
		 * If the darwin is chosen as master it will 
		 * 1. Generate the script
		 * 2. Execute move
		 * 3. Send move to slave and wait for response
		 * 4. Until script is complete jump to 2
		 */
		case 0:
			// 1. Generate the game
			generate();
			
			// Play the script
			while( 1 ) {
				
				// 2. Play the master's move
				n = nextMove( 1 );
				
				if( n == 55 ) {
					_message[0] = (char) n;
					// Send the move to the slave
					while( 1 ) {
						if ( comm_send( _message, 1, 0 ) == 0 ) {
							break;
						}
					}
					break;
				}
				
				if(n!=-1){
					playmove(n);
				}
				
				// 3. Play the slave's move
				n = nextMove( 2 );
				if( n!=-1 ) {
					_message[0] = (char) n;
					// Send the move to the slave
					while( 1 ) {
						if ( comm_send( _message, 1, 0 ) == 0 ) {
							break;
						}
					}

					// Wait for the slave to finish and respond
					while ( 1 ) {
						if ( comm_rec( _message, 1, 0 ) == 0 ) {
							break;
						}
					}
				}
			}

			break;

		/*
		 * If the darwin is chosen as slave it will
		 * 1. Wait for instruction from Master
		 * 2. Execute move
		 * 3. Send response to master
		 * 4. Until the end of the script jump back to one.
		 */
		case 1:

			while( 1 ) {
				
				// 1. Recieve message from host
				while ( 1 ) {
					if ( comm_rec( _message, 1, 0 ) == 0 ) {
						break;
					}
				}

				// 2. Execute motion
				n = (int)_message[0];
				if( n == 55 ) {
					break;
				}
				playmove( n );

				// 3. Send response to master
				while( 1 ) {
					if ( comm_send( _message, 1, 0 ) == 0 ) {
						break;
					}
				}
			}
			break;


		// In case of garbage return -2
		default:
			return -2;
	}

	// Cleanup
	
}

void playmove(int move) {

	int page;
	/**
	* This causes the darwin to point at a specific square on the board.
	* int i can be 0-8
	*  0 1 2 
	*  3 4 5 
	*  6 7 8 
	*/
	if( (move<=8) && (move>=0) ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);
		if ( is_master ) {
			comm_update_board( 1, move+1, 0 );
		} else {
			comm_update_board( 2, move+1, 0 );
		}
		page = 110 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);
	}
	
	// 21 = win
	else if( move == 21 ) {
		page = 100 + move;
				if ( is_master ) {
			comm_update_board( 1, 11, 0 );
		} else {
			comm_update_board( 2, 11, 0 );
		}
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);
	}

	// 22 = lose
	else if( move == 22 ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);	
	}
	
	// 23 = draw
	else if( move == 23 ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);	
	}

	// 24 = draw
	else if( move == 24 ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);	
	}	
	
	// 25 = CWin
	else if( move == 25 ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);	
	}
	
	// 26 = CLose
	else if( move == 26 ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);	
	}
	
	// 27 = CRand
	else if( move == 27 ) {
		page = 100 + move;
		Action::GetInstance()->Start(page);
		while(Action::GetInstance()->IsRunning()) usleep(8*1000);	
	}
}