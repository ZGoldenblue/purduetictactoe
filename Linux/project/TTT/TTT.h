#include <iostream>
#define MASTER 1
#define SLAVE 2
#define PLAYERNUM 2
#define MOVENUM 21
#define PLAYERWIN 21
#define PLAYERLOSE 22
#define PLAYERTIE1 23
#define PLAYERTIE2 24
#define CHECK_WIN 25
#define CHECK_LOSE 26
#define CHECK_RAND 27

using namespace std;

int board[9] = {0,0,0,0,0,0,0,0,0};
int script[PLAYERNUM][MOVENUM]; 
int slavepos;
int masterpos;
int moveSet = 0;

int win = 0;

void generate();
void signalQuit(int moveSet);
int chooseMove(int darwin,int move);
int checkWin(int darwin);
int checkDef(int darwin);
int randMove(int darwin);
int nextMove(int darwin);
int checkBoardWin(int darwin); //Function ret 1 if win for darwin

void generate(){
	//Fill script[][]
	for(int u = 0; u < PLAYERNUM; u++){
		for(int v = 0; v < MOVENUM; v++){
			script[u][v] = -1;
		}
	}
	 
	
	//GAMEPLAY
	int darWin = 0;
	int oppWin = 0;
	
	while( moveSet < 14 ){

		// The Master's Move
		int masterMove = chooseMove(MASTER,moveSet);
		//check for master win
		//check for opponent win
		darWin = checkBoardWin(MASTER);
		oppWin = checkBoardWin(SLAVE);
		cout << "Master Move " << masterMove << " " << darWin << " " << oppWin <<
			endl;
		if(darWin == 0 && oppWin == 0){
			board[masterMove] = MASTER;
			script[0][moveSet] = masterMove;
			moveSet++;
		}else if(darWin == 1){
			script[0][moveSet] = PLAYERWIN;
			cout << "Master Wins! " << endl;
			moveSet++;	
			//signalQuit(moveSet);
			script[0][moveSet] = 55;
			script[1][moveSet] = 55;
		}else if(oppWin == 1){
			script[0][moveSet] = PLAYERLOSE;
			cout << "Master Loses :( " << endl;
			moveSet++;
			//signalQuit(moveSet);
			script[0][moveSet]= 55;
			script[1][moveSet]= 55;
		}

		// The Slave's move
		int slaveMove = chooseMove(SLAVE,moveSet);
		//check for master win
		//check for opponent win
		darWin = checkBoardWin(SLAVE);
		oppWin = checkBoardWin(MASTER);
		cout << "Slave Move " << slaveMove << " " <<darWin<<" "<<oppWin<<endl;
		if(darWin == 0 && oppWin  == 0){
			board[slaveMove] = SLAVE;
			script[1][moveSet] = (slaveMove);
			moveSet++;
		}else if(darWin == 1){
			script[1][moveSet] = PLAYERWIN;
			cout << "Slave Wins! " << endl;
			moveSet++;
			//signalQuit(moveSet);
			script[0][moveSet]=55;
			script[1][moveSet] = 55;
		}else if(oppWin == 1){
			script[1][moveSet] = PLAYERLOSE;
			cout << "Slave Loses :( " << endl;
			moveSet++;
			//signalQuit(moveSet);
			script[0][moveSet] = 55;
			script[1][moveSet] = 55;
		}
	}
		// The Master's Move
	int masterMove = chooseMove(MASTER,moveSet);
	//check for master win
	//check for opponent win
	darWin = checkBoardWin(MASTER);
	oppWin = checkBoardWin(SLAVE);
	cout << "Master Move "<<masterMove <<" "<<darWin<<" "<<oppWin<<endl;
	if(darWin == 0 && oppWin == 0){
		board[masterMove] = MASTER;
		script[0][moveSet] = masterMove;
		moveSet++;
	}else if(darWin == 1){
		script[0][moveSet] = PLAYERWIN;
		cout << "Master Wins! " << endl;
		moveSet++;	
		//signalQuit(moveSet);
		script[0][moveSet]=55;
		script[1][moveSet] =55;
	}else if(oppWin == 1){
		script[0][moveSet] = PLAYERLOSE;
		cout << "Master Loses :( " << endl;
		moveSet++;
		cout << moveSet;
		//signalQuit(moveSet);
		script[0][moveSet] =55;
		script[1][moveSet] = 55;
	}
	//Once it gets here, the game should be over
	script[0][moveSet] = 23;
	moveSet++;
	script[1][moveSet] = 24;	
	moveSet++;
//	signalQuit(moveSet);
	cout << "MOVESET " << moveSet << endl;
	script[1][moveSet] = 55;
	script[0][moveSet] = 55;
}

void signalQuit(int moveSet){
	script[0][moveSet] = 55;
	script[1][moveSet] = 55;
}

int chooseMove(int darwin,int move){
	int scriptNum = 0;
	if(darwin == MASTER){
		scriptNum = 0;
	}else{
		scriptNum = 1;
	}	
	//Returns the appropriate move for the master or slave,
	//value indicated by "darwin";
	int boardMove = 0;
	
	// Check For a winning move
	if((boardMove = checkWin(darwin)) != -1){
		script[scriptNum][moveSet] =	CHECK_WIN;
		moveSet++;
		return boardMove;
		
	// Check for a defense against a winning move
	}else if((boardMove = checkDef(darwin)) != -1){
		script[scriptNum][moveSet] = CHECK_LOSE;
		moveSet++;
		return boardMove;
		
	// Make a random move
	}else{
		boardMove = randMove(darwin);
	}
	script[scriptNum][moveSet] = CHECK_RAND;
	moveSet++;
	return boardMove;
}

int checkWin(int darwin){
	int darwinSet[8] = {0,0,0,0,0,0,0,0};
	int blankSet[8] = {0,0,0,0,0,0,0,0};
	int blankSquare[8] = {-1,-1,-1,-1,-1,-1,-1,-1};
	//Rows
	int i;
	for(i = 0; i < 3; i++){
		if(board[i] == darwin){ darwinSet[0]++; }
		if(board[i] == 0){ blankSet[0]++; blankSquare[0] = i;}

		if(board[i+3] == darwin){ darwinSet[1]++; }
		if(board[i+3] == 0){ blankSet[1]++; blankSquare[1] = (i+3);}

		if(board[i+6] == darwin){ darwinSet[2]++; }
		if(board[i+6] == 0){ blankSet[2]++; blankSquare[2] = (i+6);}
	}
	//Cols
	for(i = 0; i < 9; i = i+3){
		if(board[i] == darwin){ darwinSet[3]++; }
		if(board[i] == 0){ blankSet[3]++; blankSquare[3] = i;}

		if(board[i+1] == darwin){ darwinSet[4]++; }
		if(board[i+1] == 0){ blankSet[4]++; blankSquare[4] = (i+1);}

		if(board[i+2] == darwin){ darwinSet[5]++; }
		if(board[i+2] == 0){ blankSet[5]++; blankSquare[5] = (i+2);}
	}
	//DiagPos, DiagNeg
	for(i = 2; i < 7; i = i+2){
		if(board[i] == darwin){ darwinSet[6]++; }
		if(board[i] == 0){ blankSet[6]++; blankSquare[6] = i;}
	}
	for(i = 0; i < 9; i = i+4){
		if(board[i] == darwin){ darwinSet[7]++; }
		if(board[i] == 0){ blankSet[7]++; blankSquare[7] = i;}
	}
	//Check for Wins
	for(i = 0; i < 8; i++){
		if(darwinSet[i] == 2 && blankSet[i] == 1){
			return blankSquare[i];
		}
	}
	return -1;
}

int checkDef(int darwin){
	//Darwin Determination
	int opponent = 0;
	if(darwin == MASTER){
		opponent = SLAVE;
	}else{
		opponent = MASTER;
	}
	
	int oppSet[8] = {0,0,0,0,0,0,0,0};
	int blankSet[8] = {0,0,0,0,0,0,0,0};
	int blankSquare[8] = {-1,-1,-1,-1,-1,-1,-1,-1};
	//Rows
	int i;
	for(i = 0; i < 3; i++){
		if(board[i] == opponent){ oppSet[0]++; }
		if(board[i] == 0){ blankSet[0]++; blankSquare[0] = i;}

		if(board[i+3] == opponent){ oppSet[1]++; }
		if(board[i+3] == 0){ blankSet[1]++; blankSquare[1] = (i+3);}

		if(board[i+6] == opponent){ oppSet[2]++; }
		if(board[i+6] == 0){ blankSet[2]++; blankSquare[2] = (i+6);}
	}
	
	//Cols
	for(i = 0; i < 9; i = i+3){
		if(board[i] == opponent){ oppSet[3]++; }
		if(board[i] == 0){ blankSet[3]++; blankSquare[3] = i;}

		if(board[i+1] == opponent){ oppSet[4]++; }
		if(board[i+1] == 0){ blankSet[4]++; blankSquare[4] = (i+1);}

		if(board[i+2] == opponent){ oppSet[5]++; }
		if(board[i+2] == 0){ blankSet[5]++; blankSquare[5] = (i+2);}
	}
	
	//DiagPos, DiagNeg
	for(i = 2; i < 7; i = i+2){
		if(board[i] == opponent){ oppSet[6]++; }
		if(board[i] == 0){ blankSet[6]++; blankSquare[6] = i;}
	}
	for(i = 0; i < 9; i = i+4){
		if(board[i] == opponent){ oppSet[7]++; }
		if(board[i] == 0){ blankSet[7]++; blankSquare[7] = i;}
	}
	
	for(i = 0; i < 8; i++){
		if(oppSet[i] == 2 && blankSet[i] == 1){
			return blankSquare[i];
		}
	}
	return -1;
}

/**
 * This function is not random... yet
 *
 */
int randMove(int darwin){
	int priority[9] = {4, 0, 2, 6, 8, 1, 3, 5, 7}; // Priority of squares when playing when playing

	for(int i=0; i<9; i++) {
		if(board[priority[i]]==0) {
			return priority[i];
	}
	}
	return -1;
}

int nextMove(int darwin) {
	int move;
	if (darwin == MASTER) {
		move = script[0][masterpos];
		masterpos++;
		return move;
	}else if (darwin == SLAVE) {
		move = script[1][slavepos];
		slavepos++;
		return move;

	}else {
		return 0;
	}
}

int checkBoardWin(int darwin){
//	cout << "HERE ";
	int darwinSet2[8] = {0,0,0,0,0,0,0,0};
	//Rows
	int i;
	for(i = 0; i < 3; i++){
		if(board[i] == darwin){ darwinSet2[0]++; }
		if(board[i+3] == darwin){ darwinSet2[1]++; }
		if(board[i+6] == darwin){ darwinSet2[2]++; }
	}
	//Cols
	for(i = 0; i < 9; i = i+3){
		if(board[i] == darwin){ darwinSet2[3]++; }
		if(board[i+1] == darwin){ darwinSet2[4]++; }
		if(board[i+2] == darwin){ darwinSet2[5]++; }
	}
	//DiagPos, DiagNeg
	for(i = 2; i < 7; i = i+2){
		if(board[i] == darwin){ darwinSet2[6]++; }
	}
	for(i = 0; i < 9; i = i+4){
		if(board[i] == darwin){ darwinSet2[7]++; }
	}
	//Check for Wins
	for(i = 0; i < 8; i++){
		if(darwinSet2[i] == 3){
		 return 1;
		}	 
	}
//	cout << "RETURNED " << endl; 
	return 0;

}

