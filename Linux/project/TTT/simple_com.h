#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>
#include "crc16.h"
#include <stdlib.h>


void comm_update_board(int color, int loc, unsigned char debug);

char ZIG_DEV[25] = {"/dev/ttyUSB1"};	// ZIGBEE Serial device to use
unsigned long int ERROR_LIMIT = 10;	// Number of errors allowed before failure is reported

unsigned char est      = 0x00;		// Value for establish message
unsigned char estACK   = 0x01;		// Value for establish message ACK
unsigned char estSyn   = 0x02;		// Value for establish message SYNACK
unsigned char dataMes  = 0x03;		// Value for data message
unsigned char dataFrag = 0x04;		// Value for data message fragmented
unsigned char dataACK  = 0x05;		// Value for data message ACK
unsigned char custMes  = 0xFF;		// Value for custom built messages

FILE * dev;                             	// FILE struct holder for ZIGBEE Serial device
unsigned char messageID   = 0x00;               // Global holder for messageID
unsigned char transaction = 0x00;               // Gloabl holder for transaction
unsigned char prevRecMessage[64] = {""};	// Store for previously received message from comm_rec
						//	This keeps from receiving multiple copies of the
						//	same message if multiple attempts where made.

unsigned char prevRecACK[64] = {""};		// Similar to store above


/*
 * Initialize the FILE handler and set it the global holder
 * 
 * Returns:
 *		0  = success
 *		-1 = failure
 */
int comm_init()
{
	// Open file handler
	FILE * usb = fopen(ZIG_DEV, "r+");
	// If handler is NULL, then there was an error
	if (usb == NULL)
	{
		printf("Failed to open %s\n", ZIG_DEV);
		return -1;
	} else {
		// Set FILE handler to global holder
		dev = usb;
		return 0;
	}
}

/*
 * Run comm_init() and set a messageID so a MASTER can be forced in the communications channel
 *
 * Returns the same as comm_init()
 *
 */
int comm_init_force(unsigned char mesID)
{
	int comRun = comm_init();

	messageID = mesID;

	return comRun;
}

/*
 * Start communication channel and return MASTER or SLAVE status
 *
 * Params:
 *		unsigned char debug - turn on debug messages with value > 0
 *
 * Returns:
 *		0  = Master
 *		1  = Slave
 *		-1 = Failure
 */
int comm_start(unsigned char debug)
{
	// Setup function level holders
	int i;
	int bm = 1;							// backoff multiplier
	unsigned long int randSeed;
	unsigned int error_count = 0;

	unsigned char prevEst1[64] = {""};	// last Establish Message sent
	unsigned char prevEst2[64] = {""};	// prior to last Establish Message sent
	unsigned char lastACK[64]  = {""};	// last ACK to be sent

	// Get random seed value from /dev/urandom
	FILE * random = fopen("/dev/urandom", "r");
	fread(&randSeed, 1, sizeof(randSeed), random);
	fclose(random);

	// Seed rand() function
	srand(randSeed);

	// Flush anything in the serial connection before starting & begin
	fflush(dev);
	while(1)
	{
		if (error_count > ERROR_LIMIT)
		{
			// There was more than 20 concecutive errors... time to give up
			if (debug)
			{
				printf("There was more than 20 errors in a row. Giving up... sorry!\n\n");
				return -1;
			}
		}

		// Get random number for backoff period
		unsigned int randNum1 = rand();
		unsigned int randNum2 = rand();
		unsigned int randNum3 = rand();

		// Set random values for time structures
		unsigned int	  bt_sec  = (((randNum1 % 900000) + 100000) * bm) / 1000000;
		unsigned long int bt_usec = (((randNum1 % 900000) + 100000) * bm) % 1000000;

		// Setup time structures
		struct timeval ack_t;
			ack_t.tv_sec = 2;
			ack_t.tv_usec = 0;
		struct timeval backoff_t;
			backoff_t.tv_sec = bt_sec;
			backoff_t.tv_usec = bt_usec;

		// DEBUG
		if (debug) { printf("Backoff Timer %d secs, %ld microsec\n", bt_sec, bt_usec); }

		// Setup file sets to allow timeout checking
		fd_set backoff_timeout;
		FD_ZERO(&backoff_timeout);
		FD_SET(fileno(dev), &backoff_timeout);

		// Clear buffer and wait for backoff period
		fflush(dev);
		int backoff_listen = select(fileno(dev)+1, &backoff_timeout, NULL, NULL, &backoff_t);
		if ( backoff_listen == 0 ) // Didn't hear anything, send message
		{
			// Build message
			unsigned char sendEst[64] = {""};

			// Set messageID and the transaction number
			messageID   = (randNum2 % 255);
			transaction = (randNum3 % 255);

			// Build message header
			sendEst[0]  = messageID;
			sendEst[1]  = est;
			sendEst[3]  = transaction;

			// Calculate & Assign CRC
			unsigned short sendCRC = crc16(sendEst, 62);
			unsigned char sendCRC1 = sendCRC & 0xFF;
			unsigned char sendCRC2 = (sendCRC >> 8) & 0xFF;
			sendEst[62] = sendCRC1;
			sendEst[63] = sendCRC2;

			// Keep last two Establish Messages to check ACKs against
			prevEst2[0] = prevEst1[0];
			prevEst2[3] = prevEst1[3];
			prevEst1[0] = sendEst[0];
			prevEst1[3] = sendEst[3];

			// Print Sent Message if debug is set
			if (debug)
			{
				printf(" - Sent:\n    ");
				for (i=0; i < sizeof(sendEst); i++)
				{
					printf("%X", sendEst[i]);
					if ( (i % 28) == 0 && i != 0) { printf("\n    "); }
				}
				printf("\n");
			}

			// Send message, close channel, wait buffer time
			fwrite(sendEst, 1, sizeof(sendEst), dev);
			fflush(dev);
			usleep(25000);

			// Wait for ACK
			unsigned char ack[64] = {""};
			
			// Read in ACK with timeout
			fd_set ack_wait_timeout;
			FD_ZERO(&ack_wait_timeout);
			FD_SET(fileno(dev), &ack_wait_timeout);

			// Wait for ACK timeout
			int ack_listen = select(fileno(dev)+1, &ack_wait_timeout, NULL, NULL, &ack_t);
			if ( ack_listen == 0 ) // No ACK received so start over
			{
				bm++;		// Increase backoff multiplier slowly to slow down messages
				error_count++;
				if (debug) { printf(" - No ACK received, starting over...\n"); }
				continue;
			} else { // Something received, check if it is an ACK
				// Read data
				fread(&ack, 1, sizeof(ack), dev);

				// Check if it is a reply to the last establish message
				if ( ack[0] == messageID && ack[1] == estACK && ack[3] == transaction )
				{
					if (debug) { printf(" - Message is an ACK, checking validity...\n"); }

					// Calculte CRC for read message to validate that message
					unsigned short ackCRC = crc16(ack, 62);
					unsigned char ackCRC1 = ackCRC & 0xFF;
					unsigned char ackCRC2 = (ackCRC >> 8) & 0xFF;

					// Check message validity
					if ( ack[62] == ackCRC1 && ack[63] == ackCRC2 ) // ACK is good, send SYNACK
					{
						// Send SYNACK
						unsigned char synack[64] = {""};
						synack[0] = messageID;
						synack[1] = estSyn;
						synack[3] = transaction;

						unsigned short synCRC = crc16(synack, 62);
						unsigned char synCRC1 = synCRC & 0xFF;
						unsigned char synCRC2 = (synCRC >> 8) & 0xFF;

						synack[62] = synCRC1;
						synack[63] = synCRC2;

						fwrite(synack, 1, sizeof(synack), dev);
						fflush(dev);
						usleep(25000);

						if (debug)
						{
							// Print the ACK message received
							printf(" - - ACK:\n      ");
							for (i = 0; i < sizeof(ack); i++)
							{
								printf("%X", ack[i]);
								if ( (i % 28) == 0 && i != 0 ) { printf("\n      "); }
							}
							printf("\n");

							// Print teh SYNACK sent
							printf(" - - - Sent SYNACK:\n        ");
							for (i = 0; i < sizeof(synack); i++)
							{
								printf("%X", synack[i]);
								if ( (i % 28) == 0 && i != 0) { printf("\n        "); }
							}
							printf("\n");
						}

						return 0; // Return MASTER
					} else {
						// Print BAD ACK if debug is set
						if (debug)
						{
							printf(" - - ACK is not valid (CRC):\n      ");
							for (i=0; i < sizeof(ack); i++)
							{
								printf("%X", ack[i]);
								if ( (i % 28) == 0 && i != 0) { printf("\n      "); }
							}
							printf(" - - - Flushing channel and starting over\n");
						}
						fflush(dev);
						error_count++;
						continue;
					}
				} else { // Message was not an ACK
					// Check if message is a SYNACK from last ACK
					unsigned short posSYNCRC = crc16(ack, 62);

					if ( ack[0] == lastACK[0] && ack[1] == estSyn && ack[3] == lastACK[3]
							&& ack[62] == (posSYNCRC & 0xFF) && ack[63] == ((posSYNCRC >> 8) & 0xFF) )
					{
						// Message is a SYNACK from previous ACK... now SLAVE
						if (debug)
						{
							printf(" - - Message was previous SYNACK\n      ");
							for (i = 0; i < sizeof(ack); i++)
							{
								printf("%X", ack[i]);
								if ( (i % 28) == 0 && i != 0 ) { printf("\n      "); }
							}
							printf("Returning as Slave.\n");
						}

						messageID = ack[0]; // Reset the messageID
						return 1; // Now SLAVE
					}


					if (debug)
					{
						printf(" - - Message received was not an ACK, starting over...\n");

						printf(" - - - Frame:\n        ");
						for (i = 0; i < sizeof(ack); i++)
						{
							printf("%X", ack[i]);
							if ( (i % 28) == 0 && i != 0 ) {printf("\n        ");}
						}
						printf("\n");
					}

					error_count++;
					bm++; // increase backoff multiplier because something is filling the line
					continue;
				}
			}
		} else {
			// Heard something, check if it is establish message
			if (debug) { printf(" - Receiving Message... Checking for Establish Message\n"); }

			unsigned char estMes[64] = {""};
			fread(&estMes, 1, sizeof(estMes), dev);
			
			if ( estMes[1] == 0x00 )
			{
				if (debug) { printf(" - - Message is an Establish Message\n"); }

				// Check if message is valid
				unsigned short estCRC = crc16(estMes, 62);
				unsigned char estCRC1 = estCRC & 0xFF;
				unsigned char estCRC2 = (estCRC >> 8) & 0xFF;

				if ( estMes[62] == estCRC1 && estMes[63] == estCRC2 ) // Est. Message valid, send ACK
				{
					// Store MessageID & Transaction
					messageID   = estMes[0];
					transaction = estMes[3];

					// Build ACK
					unsigned char ack[64] = {""};
					ack[0] = messageID;
					ack[3] = transaction;
					ack[1] = estACK;

					// Generate & assign CRC
					unsigned short ackCRC = crc16(ack, 62);
					ack[62] = ackCRC & 0xFF;
					ack[63] = (ackCRC >> 8) & 0xFF;

					// Debug info
					if (debug)
					{
						printf(" - - - Message was valid, Sending ACK...\n");
						printf(" - - - - Sent ACK:\n          ");
						for (i=0; i < sizeof(ack); i++)
						{
							printf("%X", ack[i]);
							if ( (i % 28) == 0 && i != 0) { printf("\n                   "); }
						}
						printf("\n");
					}

					// Write ACK
					fwrite(ack, 1, sizeof(ack), dev);
					fflush(dev);
					usleep(25000);

					// Store most recent ACK
					memcpy(lastACK, ack, sizeof(ack));

					// Wait for synack
					struct timeval syn_t;
						syn_t.tv_sec = 5;
						syn_t.tv_usec = 0;
					
					fd_set syn_timeout;
					FD_ZERO(&syn_timeout);
					FD_SET(fileno(dev), &syn_timeout);

					unsigned char synack[64] = {""};

					int syn_listen = select(fileno(dev)+1, &syn_timeout, NULL, NULL, &syn_t);
					if ( syn_listen == 0 )
					{
						// No SYNACK was heard, start over
						if (debug) { printf(" - - - - - No SYNACK was heard. Restarting...\n"); }

						error_count++;
						continue;
					} else {
						// Heard something, check for SYNACK
						fread(&synack, 1, sizeof(synack), dev);

						if ( synack[0] == messageID && synack[1] == estSyn && synack[3] == transaction)
						{
							// Message is a SYNACK, check validity
							unsigned short synCRC = crc16(synack, 62);
							
							if ( synack[62] == (synCRC & 0xFF) && synack[63] == ((synCRC >> 8) & 0xFF) )
							{
								// SYNACK good, return slave
								if (debug)
								{
									printf(" - - - - - SYNACK was good\n            ");
									for (i = 0; i < sizeof(synack); i++)
									{
										printf("%X", synack[i]);
										if ( (i % 28) == 0 && i != 0 ) { printf("\n            "); }
									}
									printf("\n - - - - - - Return as Slave\n\n");
								}

								return 1; // return SLAVE
							} else {
								// SYNACK Bad... return failure
								if (debug)
								{
									printf(" - - - - - SYNACK was bad. Failing.\n");
									printf(" - - - - - - SYNACK:\n                ");
									for (i = 0; i < sizeof(synack); i++)
									{
										printf("%X", synack[i]);
										if ( (i % 28) == 0 && i != 0) {printf("\n              ");}
									}
									printf("\n");
								}

								return -1; // return failure because the other side is likely out of sync with this one
							}
						} else {
							// Message was not a SYNACK
							if (debug)
							{
								printf(" - - - - Message was not an SYNACK. Start over...\n");
								printf(" - - - - - FRAME:\n            ");
								for (i = 0; i < sizeof(synack); i++)
								{
									printf("%X", synack[i]);
									if ( (i % 28) == 0 && i !=0 ) {printf("\n            ");}
								}
								printf("\n\n");
							}

							error_count++;
							continue;
						}
					}
				} else {
					if (debug)
					{
						printf(" - - Message was an establish message, but was not valid (CRCs).\n");
						printf(" - - - Frame:\n        ");
						for (i=0; i < sizeof(estMes); i++)
						{
							printf("%X", estMes[i]);
							if ( (i % 28) == 0 && i != 0) { printf("\n        "); }
						}
						printf("\n");
					}

					bm * 2;		// Increase backoff multiplier to avoid more collisions
					fclose(dev);	// Close device to clean buffer
					comm_init();	// Restart device
					sleep(1);	// Give some buffer time

					error_count++;
					continue;
				}
			} else {
				// Not an establish message so check and see if it could
				// be the ACK from the last Establish Message sent
				if ( estMes[0] == prevEst1[0] && estMes[1] == estACK && estMes[3] == prevEst1[3] )
				{
					if (debug)
					{
						printf(" - - - Message was an ACK from the last Est. Mess., return MASTER\n\n");
					}
					// Reset MessageID & Transaction code
					messageID   = prevEst1[0];
					transaction = prevEst1[3];

					return 0; // return MASTER
				} else if (estMes[0] == prevEst2[0] && estMes[1] == estACK && estMes[3] == prevEst2[3]) {
					if (debug)
					{
						printf(" - - - Message was an ACK from 2 Est. Mess. ago, return MASTER\n\n");
					}
					// Reset MessageID & Transaction code
					messageID   = prevEst2[0];
					transaction = prevEst2[3];

					return 0; // return MASTER
				} else {
					if (debug)
					{
						printf(" - - - Message received was not an establish message.\n");
						printf(" - - - - Previous Est1:\n          ");
						for (i=0; i<sizeof(estMes); i++)
						{
							printf("%X", prevEst1[i]);
							if ( (i % 28) == 0 && i != 0) {printf("\n          ");}
						}
						printf("\n");
						printf(" - - - - Previous Est2:\n          ");
						for (i=0; i<sizeof(estMes); i++)
						{
							printf("%X", prevEst2[i]);
							if ( (i % 28) == 0 && i != 0) {printf("\n          ");}
						}
						printf("\n");

						printf(" - - - - Received:\n          ");
						for (i=0; i<sizeof(estMes); i++)
						{
							printf("%X", estMes[i]);
							if ( (i % 28) == 0 && i != 0) {printf("\n          ");}
						}
						printf("\n");
					}

					error_count++;
					continue;
				}
			}
		}
	}
}


/*
 * Send data across the line
 *
 * Params:
 *		unsigned char buffer[] - array reference to were to place the data to be sent
 *		unsigned int buffSize - size of the refernce array above
 *		unsigned char debug - any value greater than 0 will cause debug messages to print
 *
 * Returns:
 *		0  - successful send
 *		-1 - failed to send message, suggest rerunning comm_start()
 */
int comm_send(unsigned char buffer[], unsigned int buffSize, unsigned char debug)
{
	if (debug) { printf("Starting send message function...\n"); }

	int i = 0;
	unsigned int error_count = 0;

	FILE * random;
	unsigned long int randSeed;
	int ack_failure = 0;

	// Fill random seed
	random = fopen("/dev/urandom", "r");
	fread(&randSeed, 1, sizeof(randSeed), random);
	fclose(random);

	// Setup transaction code
	srand( randSeed );
	transaction = (rand() % 255);

	if ( buffSize > 58 )
	{
		// do fragmentation
	} else {
		// Send single frame and wait for ACK, resend if no ack is received
		if (debug) { printf(" - Starting single frame send...\n"); }

		while (1)
		{
			if ( error_count > ERROR_LIMIT )
			{
				printf("\n\nThere was more the 20 errors in a row when sending. Giving up.... Sorry!\n\n");
				return -1;
			}

			// Build ACK timer
			struct timeval ack_t;
				ack_t.tv_sec  = 5;
				ack_t.tv_usec = 0;

			// Create frame to send
			unsigned char sendBuff[64] = {""};
			sendBuff[0] = messageID;
			sendBuff[1] = dataMes;
			sendBuff[2] = 0x00;
			sendBuff[3] = transaction;

			// copy buffer data into frame
			memcpy(sendBuff + 4, buffer, buffSize); // copy 58 bytes into 64 byte frame starting at index 4

			// Build CRC
			unsigned short crc = crc16(sendBuff, 62);
			sendBuff[62] = crc & 0xFF;
			sendBuff[63] = (crc >> 8) & 0xFF;

			// Write message to channel
			fwrite(sendBuff, 1, sizeof(sendBuff), dev);
			fflush(dev);
			usleep(25000);

			// Print send message during debug
			if (debug)
			{
				printf(" - - Message:\n      ");
				for (i = 0; i < sizeof(sendBuff); i++)
				{
					printf("%X", sendBuff[i]);
					if ( (i % 28) == 0 && i != 0 ) { printf("\n      "); }
				}
				printf("\n");
			}
			
			// Wait for ACK to message
			fd_set ack_timeout;
			FD_ZERO(&ack_timeout);
			FD_SET(fileno(dev), &ack_timeout);

			int ack_listen = select(fileno(dev)+1, &ack_timeout, NULL, NULL, &ack_t);
			if ( ack_listen == 0 )
			{
				// No ACK heard, resend message
				if (debug) { printf(" - - - No ACK heard, resending message...\n"); }

				error_count++;
				continue;
			} else {
				// Something received: check for ACK then for validity & origin
				unsigned char ack[64] = {""};

				fread(&ack, 1, sizeof(ack), dev);

				// Check for ACK & Origin
				if ( ack[0] == messageID && ack[1] == dataACK && ack[3] == transaction )
				{
					// Calculate the CRC for the ACK
					unsigned short ackCRC = crc16(ack, 62);

					// Check for ACK validity
					if ( ack[62] == (ackCRC & 0xFF) && ack[63] == ((ackCRC >> 8) & 0xFF) )
					{
						// ACK receieved and is good
						if (debug)
						{
							printf(" - - - ACK received, Message was successful.\n        ");
							for (i = 0; i < sizeof(ack); i++)
							{
								printf("%X", ack[i]);
								if ( (i % 28) == 0 && i != 0 ) { printf("\n        "); }
							}
							printf("\n");
						}

						return 0; // return successful message sent
					} else {
						// Got something but it was wrong... start over
						if (debug)
						{
							printf(" - - - Message was an ACK, but not valid (CRC).\n        ");
							for (i = 0; i < sizeof(ack); i++)
							{
								printf("%X", ack[i]);
								if ( (i % 28) == 0 && i != 0 ) { printf("\n        "); }
							}
							printf("\n");
						}

						fflush(dev);
						error_count++;
						continue;
					}
				} else {
					// Message is not an ACK, restart and resend
					if (debug)
					{
						printf(" - - - Message received was not an ACK, restarting & resending...\n");

						printf(" - - - - Frame:\n          ");
						for (i = 0; i < sizeof(ack); i++)
						{
							printf("%X", ack[i]);
							if ( (i % 28) == 0 && i != 0 ) {printf("\n          ");}
						}
						printf("\n");
					}

					error_count++;
					continue;
				}
			}
		}
	}
}


/*
 * Receive messages from channel established with comm_start() and sent from comm_send()
 *
 * Params:
 *		unsigned char buffer[] - array reference to place data received from the channel
 *		unsigned int buffSize - size of the reference array above
 *		unsigned char debug - any value greater than zero will cause debug messages to print
 *
 * Returns:
 *		 0 - messages was received successfully
 *		-1 - nothing was received or there were errors with messages, suggest rerunning comm_start()
 */
int comm_rec(unsigned char buffer[], int buffSize, unsigned char debug)
{
	if (debug) { printf("Staring receiving function...\n"); }

	int i = 0;
	unsigned int error_count = 0;

	// Check for expected fragmentation
	if ( buffSize > 58 )
	{
		// Loop for fragmentation size
	} else {
		// Single incoming frame
		while (1)
		{
			if ( error_count > ERROR_LIMIT )
			{
				printf("\n\nNothing was heard or there were errors 20 times. Giving up... Sorry!\n\n");
				return -1;
			}

			struct timeval wait_t;
				wait_t.tv_sec  = 5;
				wait_t.tv_usec = 0;

			fd_set wait_timeout;
			FD_ZERO(&wait_timeout);
			FD_SET(fileno(dev), &wait_timeout);

			fflush(dev);
			int wait_listen = select(fileno(dev)+1, &wait_timeout, NULL, NULL, &wait_t);
			if ( wait_listen == 0 )
			{
				// Nothing heard, check for retry limit and either break or continue
				if (debug) { printf(" - Nothing heard, increasing retry count...\n"); }

				error_count++;
				continue;
			} else {
				// Received something, check for message types
				unsigned char message[64] = {""};
				
				fread(&message, 1, sizeof(message), dev);

				if ( message[0] == messageID && message[1] == dataMes )
				{
					// Message is a data message, check validity
					unsigned short mCRC = crc16(message, 62);

					if ( message[62] == (mCRC & 0xFF) && message[63] == ((mCRC >> 8) & 0xFF) ) // Message was valid
					{
						// Make sure this isn't another resend of the last message recorded
						if (memcmp(prevRecMessage, message, sizeof(message)) == 0)
						{
							// Resend ACK
							fwrite(prevRecACK, 1, sizeof(prevRecACK), dev);
							fflush(dev);
							usleep(25000);

							// Message is the same as the previous... start over
							if (debug)
							{
								printf(" - Frame was the same as the last valid frame...\n");
								printf(" - - Resending previous ACK...\n");
							}

							continue;
						}

						// Read message into output buffer
						memcpy(buffer, message + 4, buffSize);

						// Build ACK
						unsigned char ack[64] = {""};
						ack[0] = message[0];
						ack[1] = dataACK;
						ack[3] = message[3];

						// Build and attach CRC
						unsigned short ackCRC = crc16(ack, 62);
						ack[62] = ackCRC & 0xFF;
						ack[63] = (ackCRC >> 8) & 0xFF;

						// Write ACK to channel
						fwrite(ack, 1, sizeof(ack), dev);
						fflush(dev);
						usleep(25000);

						if (debug)
						{
							printf(" - Valid Message received\n");

							printf(" - - Previous Frame:\n      ");
							for (i = 0; i < sizeof(prevRecMessage); i++)
							{
								printf("%X", prevRecMessage[i]);
								if ( (i % 28) == 0 && i != 0) {printf("\n      ");}
							}
							printf("\n");

							printf(" - - Frame:\n      ");
							for (i = 0; i < sizeof(message); i++)
							{
								printf("%X", message[i]);
								if ( (i % 28) == 0 && i != 0) {printf("\n      ");}
							}
							printf("\n");

							printf(" - - - Message[%d]:\n        ", buffSize);
							for (i = 0; i < buffSize; i++)
							{
								printf("%X", buffer[i]);
								if ( (i % 28) == 0 && i != 0) { printf("\n        "); }
							}
							printf("\n");
						}
						
						// Store message for later comparison
						memcpy(prevRecMessage, message, sizeof(message));
						// Store ACK for possible use if it was lost
						memcpy(prevRecACK, ack, sizeof(ack));
						
						// Return a successful message read
						return 0; // return successful read
					} else {
						// Message was not valid. Drop and listen again
						if (debug) 
						{
							printf(" - Message was not valid (CRC). Retrying...\n");
							printf(" - - Frame:\n      ");
							for (i = 0; i < sizeof(message); i++)
							{
								printf("%X", message[i]);
								if ( (i % 28) == 0 && i != 0) { printf("\n      "); }
							}
							printf("\n");
						}

						fclose(dev);
						comm_init();
						error_count++;
						continue;
					}
				} else {
					// Message was not a data message, drop and listen again
					if (debug) 
					{
						printf(" - Message was not a data message. Retrying...\n");
						printf(" - - Frame:\n      ");
						for (i = 0; i < sizeof(message); i++)
						{
							printf("%X", message[i]);
							if ( (i % 28) == 0 && i != 0) { printf("\n      "); }
						}
						printf("\n");
					}
					
					error_count++;
					continue;
				}
			}
		}	
	}
}

/*
 * Send custom message to an outside source with static protocol implementations
 *
 * This function specifically is used to send the board state to a ZigBee connected Tic-Tac-Toe board.
 *
 * Params:
 *		int color - Value of 0, 1, and 2 which represent Off, Red, Blue respectively
 *		int loc	  - Value between 1 and 11, which represent nine board positions (1-9), all 
 *				locations at once (10), and blink all locations five times (11)
 *		unsigned char debug - print debugging information
 *
 * Returns:
 *		No return
 */
void comm_update_board(int color, int loc, unsigned char debug)
{
	int i;

	// Build message
	unsigned char message[64] = {""};
	message[0] = 0x75;
	message[1] = 0xFF;
	message[4] = 0xAA;
	message[5] = (unsigned char)color << 4;
	message[5] = message[5] + (unsigned char)loc;

	// Send message 3 times
	for (i = 0; i < 3; i++)
	{
		fwrite(message, 1, sizeof(message), dev);
		fflush(dev);
		usleep(50000);
	}

	if (debug)
	{
		printf("Sending message to Tic-Tac-Toe Board 3 times\n");
		for (i = 0; i < sizeof(message); i++)
		{
			printf("%X", message[i]);
		}
		printf("\n");
	}
}

/*
 * Close the ZIGBEE device and reset messageID and transaction
 *
 */
void comm_end()
{
	if (dev != NULL)
	{
		fclose(dev);
	}

	messageID = 0;
	transaction = 0;
}
